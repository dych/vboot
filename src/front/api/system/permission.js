/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月07日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */

export default {
  list: 'system/permission/list',
  updateEnable: 'post:system/permission/updateEnable',
  save: 'post:system/permission/save',
  update: 'post:system/permission/update',
  delete: 'system/permission/delete',
  get: 'system/permission/get'
};
