package cn.gson.vboot.controller.system;

import cn.gson.vboot.common.AcLog;
import cn.gson.vboot.common.JsonResult;
import cn.gson.vboot.service.AcLogService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : cn.gson.vboot.controller.system</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年09月08日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@RequestMapping("/system/log")
public class AcLogController {

    @Autowired
    AcLogService acLogService;

    @GetMapping("/list")
    public JsonResult list(@RequestParam(defaultValue = "1") int page) {
        return JsonResult.success(new PageInfo<>(acLogService.getLogList(page)));
    }
}
